From 256f807232447b3bbad6028eb7fedf905eee5dd3 Mon Sep 17 00:00:00 2001
From: "hongchao.yin" <hongchao.yin@amlogic.com>
Date: Wed, 14 Aug 2019 11:22:33 +0800
Subject: [PATCH] BT: add a2dp out rate control [1/1]

PD#SWPL-10887

Problem:
CLONE - [MIUI][R31AD][T960X][9.0][AML-157][Feedback][Audio]:Bluetooth
connection mi soundbar sound painting is out of sync, shooting screen
and STB is obvious.[100%]

Solution:
add ring buffer for audio out stream rate control

Verify:
marconi

Change-Id: I0dbea0847f2fa26716c6167bec58bd1388b6c2dd
Signed-off-by: hongchao.yin <hongchao.yin@amlogic.com>
---
 audio_a2dp_hw/src/audio_a2dp_hw.cc | 464 ++++++++++++++++++++++++++++++++++++-
 1 file changed, 462 insertions(+), 2 deletions(-)

diff --git a/audio_a2dp_hw/src/audio_a2dp_hw.cc b/audio_a2dp_hw/src/audio_a2dp_hw.cc
index c885e2e..8a198e8 100644
--- a/audio_a2dp_hw/src/audio_a2dp_hw.cc
+++ b/audio_a2dp_hw/src/audio_a2dp_hw.cc
@@ -114,6 +114,15 @@ struct a2dp_audio_device {
 
 };
 
+typedef struct ring_buffer {
+    pthread_mutex_t lock;
+    unsigned char *start_addr;
+    unsigned char *rd;
+    unsigned char *wr;
+    int size;
+    int last_is_write;
+}ring_buffer_t;
+
 struct a2dp_config {
   uint32_t rate;
   uint32_t channel_mask;
@@ -144,6 +153,9 @@ struct a2dp_stream_out {
   int bt_unmute;
 #endif
 
+    ring_buffer_t  stRingBuf;
+    bool  bExitThread;
+    pthread_t threadID;
 };
 
 struct a2dp_stream_in {
@@ -922,17 +934,424 @@ static int suspend_audio_datapath(struct a2dp_stream_common* common,
   return 0;
 }
 
+
+#include <stdio.h>
+#include <stdlib.h>
+#include <pthread.h>
+#include <errno.h>
+#include <string.h>
+#include <cutils/log.h>
+#include <pthread.h>
+
+#define UNCOVER_WRITE    0
+#define COVER_WRITE      1
+
+/*************************************************
+Function: get_write_space
+Description: get the space can be written
+Input: write_point: write pointer
+       read_point:  read pointer
+       buffer_size: total buffer size
+Output:
+Return: the space can be written in byte
+*************************************************/
+static int get_write_space(unsigned char *write_point, unsigned char *read_point,
+        int buffer_size, int last_is_write)
+{
+    int bytes = 0;
+
+    if (write_point > read_point) {
+        bytes = buffer_size + read_point - write_point;
+    } else if (write_point < read_point) {
+        bytes = read_point - write_point;
+    } else if (!last_is_write) {
+        bytes = buffer_size;
+    }
+
+    return bytes;
+}
+
+/*************************************************
+Function: get_read_space
+Description: get the date space can be read
+Input: write_point: write pointer
+       read_point:  read pointer
+       buffer_size: total buffer size
+Output:
+Return: the data space can be read in byte
+*************************************************/
+static size_t get_read_space(unsigned char *write_point, unsigned char *read_point,
+        int buffer_size, int last_is_write) {
+    int bytes = 0;
+
+    if (write_point > read_point) {
+        bytes = write_point - read_point;
+    } else if (write_point < read_point) {
+        bytes = buffer_size + write_point - read_point;
+    } else if (last_is_write) {
+        bytes = buffer_size;
+    }
+
+    return bytes;
+}
+
+/*************************************************
+Function: write_to_buffer
+Description: write data to ring buffer
+Input: current_pointer: the current write pointer
+       data:  data to be written
+       bytes: the space of data to be written
+       start_addr: dest buffer
+       total_size: dest buffer size
+Output:
+Return: 0 for success
+*************************************************/
+static int write_to_buffer(unsigned char *current_pointer, unsigned char *data, int bytes,
+        unsigned char *start_addr, int total_size)
+{
+    int left_bytes = start_addr + total_size - current_pointer;
+
+    if (left_bytes >= bytes) {
+        memcpy(current_pointer, data, bytes);
+    } else {
+        memcpy(current_pointer, data, left_bytes);
+        memcpy(start_addr, data + left_bytes, bytes - left_bytes);
+    }
+
+    return 0;
+}
+
+/*************************************************
+Function: read_from_buffer
+Description: read data to ring buffer
+Input: current_pointer: the current read pointer
+       buffer:  buffer for the data to be read
+       bytes: the space of data to be read
+       start_addr: dest buffer
+       total_size: dest buffer size
+Output: read data
+Return: 0 for success
+*************************************************/
+static int read_from_buffer(unsigned char *current_pointer, unsigned char *buffer, int bytes,
+        unsigned char *start_addr, int total_size)
+{
+    int left_bytes = start_addr + total_size - current_pointer;
+
+    if (left_bytes >= bytes) {
+        memcpy(buffer, current_pointer, bytes);
+    } else {
+        memcpy(buffer, current_pointer, left_bytes);
+        memcpy(buffer + left_bytes, start_addr, bytes - left_bytes);
+    }
+
+    return 0;
+}
+
+/*************************************************
+Function: update_pointer
+Description: update read/write pointer of ring buffer
+Input: current_pointer: the current read/write pointer
+       bytes: data space has been written/read
+       start_addr: ring buffer
+       total_size: ring buffer size
+Output:
+Return: the updated pointer
+*************************************************/
+static inline void* update_pointer(unsigned char *current_pointer, int bytes,
+        unsigned char *start_addr, int total_size)
+{
+    current_pointer += bytes;
+
+    if (current_pointer >= start_addr + total_size) {
+        current_pointer -= total_size;
+    }
+
+    return current_pointer;
+}
+
+/*************************************************
+Function: ring_buffer_write
+Description: write data to ring buffer
+Input: rbuffer: the dest ring buffer
+       data: data to be written
+       bytes: data space in byte
+       cover: whether or not to cover the data if over run
+Output:
+Return: data space has been written
+*************************************************/
+size_t ring_buffer_write(struct ring_buffer *rbuffer, unsigned char* data, size_t bytes, int cover)
+{
+    struct ring_buffer *buf = rbuffer;
+    size_t write_space, written_bytes;
+
+    pthread_mutex_lock(&buf->lock);
+
+    if (buf->start_addr == NULL || buf->rd == NULL || buf->wr == NULL || buf->size == 0) {
+        ALOGE("%s, Buffer malloc fail!\n", __FUNCTION__);
+        pthread_mutex_unlock(&buf->lock);
+        return 0;
+    }
+
+    write_space = get_write_space(buf->wr, buf->rd, buf->size, buf->last_is_write);
+    if (write_space < bytes) {
+        if (UNCOVER_WRITE == cover) {
+            written_bytes = write_space;
+        } else {
+            written_bytes = bytes;
+        }
+    } else {
+        written_bytes = bytes;
+    }
+
+    write_to_buffer(buf->wr, data, written_bytes, buf->start_addr, buf->size);
+    buf->wr = (unsigned char*)update_pointer(buf->wr, written_bytes, buf->start_addr, buf->size);
+    if (written_bytes)
+        buf->last_is_write = 1;
+
+    pthread_mutex_unlock(&buf->lock);
+
+    return written_bytes;
+}
+
+/*************************************************
+Function: ring_buffer_read
+Description: read data from ring buffer
+Input: rbuffer: the source ring buffer
+       buffer: buffer for the read data
+       bytes: data space in byte
+Output: data has been read
+Return: data space has been read
+*************************************************/
+size_t ring_buffer_read(struct ring_buffer *rbuffer, unsigned char* buffer, size_t bytes)
+{
+    struct ring_buffer *buf = rbuffer;
+    size_t readable_space, read_bytes;
+
+    pthread_mutex_lock(&buf->lock);
+
+    if (buf->start_addr == NULL || buf->rd == NULL || buf->wr == NULL
+            || buf->size == 0) {
+        ALOGE("%s, Buffer malloc fail!\n", __FUNCTION__);
+        pthread_mutex_unlock(&buf->lock);
+        return 0;
+    }
+
+    readable_space = get_read_space(buf->wr, buf->rd, buf->size, buf->last_is_write);
+    if (readable_space < bytes) {
+        read_bytes = readable_space;
+    } else {
+        read_bytes = bytes;
+    }
+
+    read_from_buffer(buf->rd, buffer, read_bytes, buf->start_addr, buf->size);
+    buf->rd = (unsigned char*)update_pointer(buf->rd, read_bytes, buf->start_addr, buf->size);
+    if (read_bytes)
+        buf->last_is_write = 0;
+    pthread_mutex_unlock(&buf->lock);
+
+    return read_bytes;
+}
+
+/*************************************************
+Function: ring_buffer_init
+Description: initialize ring buffer
+Input: rbuffer: the ring buffer to be initialized
+       buffer_size: total size of ring buffer
+Output:
+Return: 0 for success, otherwise fail
+*************************************************/
+int ring_buffer_init(struct ring_buffer *rbuffer, int buffer_size)
+{
+    struct ring_buffer *buf = rbuffer;
+
+    pthread_mutex_lock(&buf->lock);
+
+    buf->size = buffer_size;
+    buf->start_addr = (unsigned char*)malloc(buffer_size * sizeof(unsigned char));
+    if (buf->start_addr == NULL) {
+        ALOGD("%s, Malloc android out buffer error!\n", __FUNCTION__);
+        pthread_mutex_unlock(&buf->lock);
+        return -1;
+    }
+
+    memset(buf->start_addr, 0, buffer_size);
+    buf->rd = buf->start_addr;
+    buf->wr = buf->start_addr;
+    buf->last_is_write = 0;
+    pthread_mutex_unlock(&buf->lock);
+
+    return 0;
+}
+
+/*************************************************
+Function: ring_buffer_release
+Description: release ring buffer
+Input: rbuffer: the ring buffer to be released
+Output:
+Return: 0 for success, otherwise fail
+*************************************************/
+int ring_buffer_release(struct ring_buffer *rbuffer)
+{
+    struct ring_buffer *buf = rbuffer;
+
+    pthread_mutex_lock(&buf->lock);
+
+    if (buf->start_addr != NULL) {
+        free(buf->start_addr);
+        buf->start_addr = NULL;
+    }
+
+    buf->rd = NULL;
+    buf->wr = NULL;
+    buf->size = 0;
+    buf->last_is_write = 0;
+
+    pthread_mutex_unlock(&buf->lock);
+
+    return 0;
+}
+
+/*************************************************
+Function: ring_buffer_reset
+Description: reset ring buffer
+Input: rbuffer: the ring buffer to be reset
+Output:
+Return: 0 for success, otherwise fail
+*************************************************/
+int ring_buffer_reset(struct ring_buffer *rbuffer)
+{
+    struct ring_buffer *buf = rbuffer;
+
+    pthread_mutex_lock(&buf->lock);
+    memset(buf->start_addr, 0, buf->size);
+    buf->rd = buf->wr = buf->start_addr;
+    buf->last_is_write = 0;
+    /*
+     * if (buf->rd >= (buf->start_addr + buf->size))
+     *    buf->rd -= buf->size;
+     */
+    pthread_mutex_unlock(&buf->lock);
+
+    return 0;
+}
+
+/*************************************************
+Function: ring_buffer_reset_size
+Description: reset ring buffer and change the size
+Input: rbuffer: the ring buffer to be reset
+       buffer_size: new size for ring buffer
+Output:
+Return: 0 for success, otherwise fail
+*************************************************/
+int ring_buffer_reset_size(struct ring_buffer *rbuffer, int buffer_size)
+{
+    if (buffer_size > rbuffer->size) {
+        ALOGW("resized buffer size exceed largest buffer size, max %d, cur %d\n", \
+              rbuffer->size, buffer_size);
+        ring_buffer_release(rbuffer);
+        rbuffer->size = buffer_size;
+        return ring_buffer_init(rbuffer, buffer_size);
+    }
+
+    ALOGI("reset buffer size from %d to %d\n", rbuffer->size, buffer_size);
+
+    pthread_mutex_lock(&rbuffer->lock);
+
+    rbuffer->size = buffer_size;
+    memset(rbuffer->start_addr, 0, buffer_size);
+    rbuffer->rd = rbuffer->start_addr;
+    rbuffer->wr = rbuffer->start_addr;
+
+    pthread_mutex_unlock(&rbuffer->lock);
+
+    return 0;
+}
+
+/*************************************************
+Function: get_buffer_read_space
+Description: get the data space for reading in the buffer
+Input: rbuffer: the ring buffer with data
+Output:
+Return: data space for success, otherwise < 0
+*************************************************/
+int get_buffer_read_space(struct ring_buffer *rbuffer)
+{
+    size_t read_space = 0;
+
+    pthread_mutex_lock(&rbuffer->lock);
+    if (rbuffer->start_addr == NULL || rbuffer->wr == NULL || rbuffer->rd == NULL
+            || rbuffer->size == 0) {
+        ALOGE("%s, Buffer malloc fail!\n", __FUNCTION__);
+        pthread_mutex_unlock(&rbuffer->lock);
+        return -1;
+    }
+
+    read_space = get_read_space(rbuffer->wr, rbuffer->rd, rbuffer->size, rbuffer->last_is_write);
+    pthread_mutex_unlock(&rbuffer->lock);
+
+    return read_space;
+}
+
+/*************************************************
+Function: get_buffer_write_space
+Description: get the space for writing in the buffer
+Input: rbuffer: the ring buffer to be written
+Output:
+Return: data space for success, otherwise < 0
+*************************************************/
+int get_buffer_write_space(struct ring_buffer *rbuffer)
+{
+    size_t write_space = 0;
+
+    pthread_mutex_lock(&rbuffer->lock);
+    if (rbuffer->start_addr == NULL || rbuffer->wr == NULL || rbuffer->wr == NULL
+        || rbuffer->size == 0) {
+        ALOGE("%s, Buffer malloc fail!\n", __FUNCTION__);
+        pthread_mutex_unlock(&rbuffer->lock);
+        return -1;
+    }
+
+    write_space = get_write_space(rbuffer->wr, rbuffer->rd, rbuffer->size, rbuffer->last_is_write);
+    pthread_mutex_unlock(&rbuffer->lock);
+
+    return write_space;
+}
+
+#define THREAD_WRITE_BYTE_SIZE (1024)
+
+void *threadloop(void *data) {
+    struct a2dp_stream_out* out = (struct a2dp_stream_out*)data;
+
+    unsigned char *pBuf = (unsigned char *) malloc (THREAD_WRITE_BYTE_SIZE);
+
+    while(false == out->bExitThread){
+        int s32FreeSize = get_buffer_read_space(&out->stRingBuf);
+        if (s32FreeSize < THREAD_WRITE_BYTE_SIZE) {
+            continue;
+        }
+        ring_buffer_read(&out->stRingBuf, pBuf, THREAD_WRITE_BYTE_SIZE);
+        skt_write(out->common.audio_fd, pBuf, THREAD_WRITE_BYTE_SIZE);
+    }
+    free(pBuf);
+    return NULL;
+}
+
 /*****************************************************************************
  *
  *  audio output callbacks
  *
  ****************************************************************************/
-
 static ssize_t out_write(struct audio_stream_out* stream, const void* buffer,
                          size_t bytes) {
   struct a2dp_stream_out* out = (struct a2dp_stream_out*)stream;
   int sent = -1;
   size_t write_bytes = bytes;
+
+    static long s64RemainFrameSize = 0;
+    long s64AdjFrameSize = 0;
+    long long s64TimeUs = 0;
+
+
 #if defined(AUDIO_EFFECT_EXTERN_DEVICE)
   int16_t *outbuff;
   int32_t *outbuff1;
@@ -1016,7 +1435,38 @@ static ssize_t out_write(struct audio_stream_out* stream, const void* buffer,
     }
   }
 #endif
-  sent = skt_write(out->common.audio_fd, buffer, write_bytes);
+
+    {
+        struct timespec currTime;
+        static struct timespec preTime;
+        long long u64CurTimeUs = 0;
+        long long u64PreTimeUs = 0;
+
+        clock_gettime(CLOCK_MONOTONIC, &currTime);
+        u64CurTimeUs = currTime.tv_sec * 1000000 + currTime.tv_nsec/1000;
+        u64PreTimeUs = preTime.tv_sec * 1000000 + preTime.tv_nsec/1000;
+        // A period of time (us). eg: 44.1k s64TimeUs = 20317 us;
+        s64TimeUs = (bytes*1000*1000/audio_stream_out_frame_size(stream))/44100;
+
+        if ((u64CurTimeUs-u64PreTimeUs) < s64TimeUs) {
+            long long u64SleepTimeUs = s64TimeUs-(u64CurTimeUs-u64PreTimeUs);
+            s64AdjFrameSize = u64SleepTimeUs*44100/1000000;
+            s64RemainFrameSize += s64AdjFrameSize;
+            if (u64SleepTimeUs<20000) {
+                usleep(u64SleepTimeUs);
+            }
+        } else {
+            long long u64SleepTimeUs = s64TimeUs-(u64CurTimeUs-u64PreTimeUs);
+            s64AdjFrameSize = u64SleepTimeUs*44100/1000000;
+            s64RemainFrameSize += s64AdjFrameSize;
+        }
+        preTime = currTime;
+    }
+
+    ring_buffer_write(&out->stRingBuf, (unsigned char*)buffer, write_bytes, UNCOVER_WRITE);
+    usleep(s64TimeUs-1000);
+    sent = write_bytes;
+
   lock.lock();
 
   if (sent == -1) {
@@ -1725,6 +2175,11 @@ static int adev_open_output_stream(struct audio_hw_device* dev,
   a2dp_dev->output->left_gain = 1;
   a2dp_dev->output->right_gain = 1;
 #endif
+
+  out->bExitThread = false;
+  ring_buffer_init(&out->stRingBuf, 1024*8);
+  pthread_create(&out->threadID, NULL, &threadloop, out);
+
   DEBUG("success");
   /* Delay to ensure Headset is in proper state when START is initiated from
    * DUT immediately after the connection due to ongoing music playback. */
@@ -1764,6 +2219,10 @@ static void adev_close_output_stream(struct audio_hw_device* dev,
   free(stream);
   a2dp_dev->output = NULL;
 
+  out->bExitThread = true;
+  pthread_join(out->threadID, NULL);
+  ring_buffer_release(&out->stRingBuf);
+
   DEBUG("done");
 }
 
@@ -1982,6 +2441,7 @@ static int adev_close(hw_device_t* device) {
   delete a2dp_dev->mutex;
   a2dp_dev->mutex = nullptr;
   free(device);
+
   return 0;
 }
 
-- 
1.9.1

